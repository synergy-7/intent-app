package com.example.intentapp

import android.content.Intent
import android.content.Intent.ACTION_VIEW
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import com.example.intentapp.databinding.ActivityMain2Binding
import com.example.intentapp.databinding.ActivityMainBinding

class MainActivity2 : AppCompatActivity() {
    private lateinit var binding: ActivityMain2Binding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMain2Binding.inflate(layoutInflater)
        setContentView(binding.root)

        //Jika menggunakan putExtra maka pengambilan datanya seperti berikut
        val textOpsi1 = intent.getStringExtra(MainActivity.KEY_TEXT)
        val dataClassOpsi1 = intent.getParcelableExtra<SimpleDataClass>(MainActivity.KEY_DATA_TEXT)

        //Jika menggunakan bundle maka pengambilan datanya seperti berikut
        val textOpsi2 = intent.extras?.getString(MainActivity.KEY_TEXT_BUNDLE)
        val dataClassOpsi2 = intent.extras?.getParcelable<SimpleDataClass>(MainActivity.KEY_DATA_TEXT)

        /*
        * Anda bisa mengecek hasilnya dengan memberikan log
        * */
        Log.e("SimplePassData", textOpsi1.toString())
        Log.e("SimplePassData", textOpsi2.toString())
        Log.e("SimplePassData", dataClassOpsi1.toString())
        Log.e("SimplePassData", dataClassOpsi2.toString())

        binding.text2.text = dataClassOpsi1?.text

        /*
        * Materi selanjutnya anda bisa membuka aplikasi lain dari aplikasi kita
        * ini disebut sebagai Intent Implicit
        * caranya sebagai berikut
        * */
        binding.btnOpenWeb.setOnClickListener {
            val intent = Intent(ACTION_VIEW)
            intent.setData(Uri.parse("https://www.google.com"))
            startActivity(intent)
        }
    }
}