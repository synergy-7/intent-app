package com.example.intentapp

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class SimpleDataClass(
    val text: String
): Parcelable
