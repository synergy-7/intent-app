package com.example.intentapp

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.intentapp.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {

    //binding activity using viewbinding
    private lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        /*
        * Untuk pindah halaman bisa gunakan Intent
        * */
        binding.btnNext.setOnClickListener {
            val intent = Intent(this, MainActivity2::class.java)
            startActivity(intent)
        }

        /*
        * Untuk membawa data ketika pindah halaman
        * bisa gunakan Intent namun harus
        * ada object yang dibawa.
        * sebagai contoh: object yang dibawa adalah text
        * */
        val text = "Sejarah Dunia Bola"
        val dataClass = SimpleDataClass(text)

        /*
        * Untuk membawa data anda bisa menggunakan beberapa opsi
        * Opsi 1: PutExtra
        * Opsi 2: Bundle
        *
        * Adapun tipe data yang bisa dibawa adalah
        * 1. semua tipe data primitif (val text merupakan string, implementasi passing datanya lihat baris 47 dan baris 52)
        * 2. data class (val dataClass menggunakan parcelable, implementasi passing datanya lihat baris 48 dan baris 53)
        * */
        binding.btnNextPassData.setOnClickListener {
            val intent = Intent(this, MainActivity2::class.java)
            //opsi 1
            intent.putExtra(KEY_TEXT, text)
            intent.putExtra(KEY_DATA_TEXT, dataClass)

            //opsi 2
            val bundle = Bundle()
            bundle.putString(KEY_TEXT_BUNDLE, text)
            bundle.putParcelable(KEY_DATA_TEXT_BUNDLE, dataClass)
            intent.putExtras(bundle)

            startActivity(intent)
        }

    }

    companion object {
        const val KEY_TEXT = "KEY_TEXT"
        const val KEY_DATA_TEXT = "KEY_DATA_TEXT"
        const val KEY_TEXT_BUNDLE = "KEY_TEXT_BUNDLE"
        const val KEY_DATA_TEXT_BUNDLE = "KEY_DATA_TEXT_BUNDLE"
    }
}